package com.playtech.common;

public enum CardRank {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN;

    public static int getValue(CardRank rank) {
        int value;

        switch (rank) {
            case TWO: value = 2;
                break;
            case THREE: value = 3;
                break;
            case FOUR: value = 4;
                break;
            case FIVE: value = 5;
                break;
            case SIX: value = 6;
                break;
            case SEVEN: value = 7;
                break;
            case EIGHT: value = 8;
                break;
            case NINE: value = 9;
                break;
            case TEN: value = 10;
                break;
            default: value = 0;
        }

        return value;
    }
}
