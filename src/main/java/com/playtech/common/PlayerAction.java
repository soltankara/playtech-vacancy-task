package com.playtech.common;

public enum PlayerAction {
    HIGHER,
    LOWER,
    EQUALS;

    public static boolean isExist(PlayerAction action) {
        if (action.equals(PlayerAction.LOWER) || action.equals(PlayerAction.EQUALS) || action.equals(PlayerAction.HIGHER))
            return true;
        else return false;
    }
}
