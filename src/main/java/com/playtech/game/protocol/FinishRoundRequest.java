package com.playtech.game.protocol;

import com.playtech.common.Card;

import java.io.Serializable;

/**
 * Server notifies client that round is finished
 */
public final class FinishRoundRequest implements Serializable {
    private final long roundId;
    private final boolean win;
    private final Card card;

    public FinishRoundRequest(long roundId, boolean win, Card card) {
        this.roundId = roundId;
        this.win = win;
        this.card = card;
    }

    public long getRoundId() {
        return roundId;
    }

    public boolean isWin() {
        return win;
    }

    public Card getCard() {
        return card;
    }

    @Override
    public String toString() {
        return "FinishRoundRequest{" +
                "roundId=" + roundId +
                ", win=" + win +
                ", card=" + card +
                '}';
    }
}
