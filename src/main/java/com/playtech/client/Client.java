package com.playtech.client;

import com.playtech.client.api.GameClient;
import com.playtech.client.impl.GameClientImpl;
import com.playtech.common.Card;
import com.playtech.common.PlayerAction;
import com.playtech.game.protocol.FinishRoundRequest;
import com.playtech.game.protocol.PlayerActionRequest;
import com.playtech.game.protocol.StartRoundRequest;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Client {
    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private static GameClient gameClient;
    private static StartRoundRequest startRoundRequest;
    private static FinishRoundRequest finishRoundRequest;
    private static long roundID;
    private static long timeStamp;
    private static Card card;
    private static Date date;
    private static int actionRoundDuration;
    private static String str = "";
    private static TimerTask task;
    private static Timer timer;

    public Client() {
    }
    private void run() throws ClassNotFoundException {
        try {
            connectToPort();
            gameClient = new GameClientImpl();
            while (true) {
                StartRoundRequest startRoundRequest = (StartRoundRequest) in.readObject();
                gameClient.startRound(startRoundRequest);
                timer = new Timer();
                startTimer(timer, startRoundRequest.getActionRoundDuration());
                PlayerActionRequest request = guess();
                sendPlayerActionRequest(request);
                str = "";
                FinishRoundRequest finishRoundRequest = (FinishRoundRequest) in.readObject();
                cancelTimer(timer);
                gameClient.finishRound(finishRoundRequest);
            }

        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            try {
                close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
    private void connectToPort() throws IOException {
        requestSocket = new Socket("localhost", 2004);
        nullEverything();
        System.out.println("Connected to localhost in port 2004");
        out = new ObjectOutputStream(requestSocket.getOutputStream());
        out.flush();
        in = new ObjectInputStream(requestSocket.getInputStream());
    }
    private PlayerActionRequest guess() {
        System.out.println("Please, make your choise in 10 seconds..");
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("You are allowed to type one of (HIGHER, EQUALS, LOWER)");
            str = scanner.next();
        } while (!str.toUpperCase().equals(String.valueOf(PlayerAction.HIGHER)) &&
                !str.toUpperCase().equals(String.valueOf(PlayerAction.EQUALS)) &&
                !str.toUpperCase().equals(String.valueOf(PlayerAction.LOWER)));

        return new PlayerActionRequest(PlayerAction.valueOf(str.toUpperCase()));
    }
    private void startTimer(Timer timer, long duration) {
        task = new TimerTask() {
            public void run() {
                if (str.equals("")) {
                    FinishRoundRequest finishRoundRequest = null;
                    try {
                        finishRoundRequest = (FinishRoundRequest) in.readObject();
                        gameClient.finishRound(finishRoundRequest);
                        System.exit(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        timer.schedule( task, duration*1000 );
    }
    private void cancelTimer(Timer timer) {
        timer.cancel();
    }
    private void sendPlayerActionRequest(PlayerActionRequest playerActionRequest) {
        try {
            out.writeObject(playerActionRequest);
            out.flush();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
    private void close() throws IOException {
        in.close();
        out.close();
        requestSocket.close();
    }
    private void nullEverything() {
        startRoundRequest = null;
        finishRoundRequest = null;
        roundID = 0;
        timeStamp = 0;
        card = null;
        date = null;
        actionRoundDuration = 0;
        str = "";
        task = null;
    }
    public static void main(String args[]) throws ClassNotFoundException {
        Client client = new Client();
        client.run();
    }
}
