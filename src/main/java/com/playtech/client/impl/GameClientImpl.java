package com.playtech.client.impl;

import com.playtech.client.api.GameClient;
import com.playtech.common.Card;
import com.playtech.common.CardRank;
import com.playtech.common.CardSuit;
import com.playtech.game.protocol.FinishRoundRequest;
import com.playtech.game.protocol.StartRoundRequest;

public class GameClientImpl implements GameClient {

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\033[0;32m";

    @Override
    public void startRound(StartRoundRequest startRoundRequest) {
        System.out.println(" ");
        System.out.println("---START");
        Card card = startRoundRequest.getBaseCard();
        CardSuit cardSuit = card.getSuit();
        CardRank cardRank = card.getValue();
        System.out.println("Base Card - " + cardRank + " of " + cardSuit);
    }

    @Override
    public void finishRound(FinishRoundRequest finishRoundRequest) {
        long roundID = finishRoundRequest.getRoundId();
        boolean win = finishRoundRequest.isWin();
        Card card = finishRoundRequest.getCard();

        if (card != null){
            CardSuit cardSuit = card.getSuit();
            CardRank cardRank = card.getValue();
            System.out.println("Next Card - " + cardRank + " of " + cardSuit);
        }

        if (win){
            System.out.println(ANSI_GREEN + "ROUND : " + roundID + " YOU WON" + ANSI_RESET);
            System.out.println("---END");
        } else {
            System.out.println(ANSI_RED + "ROUND : " + roundID + " YOU LOST" + ANSI_RESET);
            System.out.println("---END");
        }
    }
}
