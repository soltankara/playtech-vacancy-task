package com.playtech.server.impl;

import com.playtech.game.protocol.PlayerActionRequest;
import com.playtech.game.protocol.PlayerActionResponse;
import com.playtech.server.api.GameService;
import com.playtech.server.api.SetBaseCardRequest;
import com.playtech.server.api.SetBaseCardResponse;

public class GameServiceImpl implements GameService {
    @Override
    public PlayerActionResponse playerAction(PlayerActionRequest playerActionRequest) {
        return new PlayerActionResponse("Unknown Player Action");
    }

    @Override
    public SetBaseCardResponse setBaseCard(SetBaseCardRequest setBaseCardRequest) {
        return new SetBaseCardResponse("");
    }
}
