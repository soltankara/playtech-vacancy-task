package com.playtech.server;

import com.playtech.common.Card;
import com.playtech.common.CardRank;
import com.playtech.common.PlayerAction;
import com.playtech.game.protocol.FinishRoundRequest;
import com.playtech.game.protocol.PlayerActionRequest;
import com.playtech.game.protocol.StartRoundRequest;
import com.playtech.server.api.GameService;
import com.playtech.server.impl.GameServiceImpl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Server extends Thread {
    protected Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private static TimerTask task;
    private static Timer timer;
    private static PlayerActionRequest playerActionRequest;
    private static long roundID = 0;
    private Card baseCard = getRandomCard();
    private Card nextCard = getRandomCard();
    private static GameService gameService;

    public Server(Socket clientSocket) throws IOException {
        this.socket = clientSocket;
        out = new ObjectOutputStream(socket.getOutputStream());
        out.flush();
        in = new ObjectInputStream(socket.getInputStream());
        roundID = 0;
        playerActionRequest = null;
        gameService = new GameServiceImpl();
    }

    public void run() {
        try {
            while (true) {
                int baseCardRank = CardRank.getValue(baseCard.getValue());
                int nextCardRank = CardRank.getValue(nextCard.getValue());
                boolean isWin = false;
                timer = new Timer();
                startRound(10, new Date().getTime(), ++roundID, baseCard);
                startTimer(timer, 10);
                playerActionRequest = (PlayerActionRequest) in.readObject();
                PlayerAction playerAction = playerActionRequest.getPlayerAction();
                if (PlayerAction.isExist(playerAction)){
                    if ((playerAction.equals(PlayerAction.LOWER) && nextCardRank < baseCardRank) ||
                            (playerAction.equals(PlayerAction.EQUALS) && nextCardRank == baseCardRank) ||
                            (playerAction.equals(PlayerAction.HIGHER) && nextCardRank > baseCardRank)) {
                        isWin = true;
                    }
                    endRound(roundID, isWin, nextCard);
                    baseCard = nextCard;
                    nextCard = getRandomCard();
                } else {
                    gameService.playerAction(playerActionRequest);
                }
                playerActionRequest = null;
                cancelTimer(timer);
            }
        } catch (IOException ioException) {
            System.out.println("Connection of "+ socket.getRemoteSocketAddress().toString() +" lost!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
    private void startTimer(Timer timer, long duration) {
        task = new TimerTask() {
            public void run() {
                System.out.println("playerActionRequest = " + playerActionRequest);
                if (playerActionRequest == null) {
                    endRound(roundID, false, nextCard);
                }
            }
        };
        timer.schedule( task, duration*1000 );
    }
    private void cancelTimer(Timer timer) {
        timer.cancel();
    }
    private void startRound(int actionRoundDuration, long actionRoundStartTimestamp, long roundId, Card baseCard) {
        StartRoundRequest startRoundRequest = new StartRoundRequest(actionRoundDuration, actionRoundStartTimestamp, roundId, baseCard);
        sendStartRoundRequest(startRoundRequest);
    }
    private void sendStartRoundRequest(StartRoundRequest startRoundRequest) {
        try {
            out.writeObject(startRoundRequest);
            out.flush();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

    }
    private void endRound(long roundId, boolean isWin, Card card) {
        FinishRoundRequest finishRoundRequest = new FinishRoundRequest(roundId, isWin, card);
        sendFinishRoundRequest(finishRoundRequest);
    }
    private void sendFinishRoundRequest(FinishRoundRequest finishRoundRequest) {
        try {
            out.writeObject(finishRoundRequest);
            out.flush();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
    private Card getRandomCard() {
        return Card.randomCard();
    }
    private void close() throws IOException {
        in.close();
        out.close();
    }
}
