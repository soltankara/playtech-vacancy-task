package com.playtech;

import com.playtech.client.api.GameClient;
import com.playtech.client.impl.GameClientImpl;
import com.playtech.common.Card;
import com.playtech.common.CardRank;
import com.playtech.common.CardSuit;
import com.playtech.common.PlayerAction;
import com.playtech.game.protocol.FinishRoundRequest;
import com.playtech.game.protocol.PlayerActionRequest;
import com.playtech.game.protocol.StartRoundRequest;
import com.playtech.server.ServerRunner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

public class SampleUnitTest {

    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    private static GameClient gameClient;
    private static StartRoundRequest startRoundRequest;
    private static FinishRoundRequest finishRoundRequest;
    private static long roundID;
    private static long timeStamp;
    private static Card card;
    private static Date date;
    private static int actionRoundDuration;
    private int baseCardRankValue, nextCardRankValue;
    private FinishRoundRequest finishRequest;

    @Before
    public void before() throws IOException, ClassNotFoundException {
        ServerRunner serverRunner = new ServerRunner();
        Thread myThread = new Thread() {
            public void run() {
                try {
                    serverRunner.main(new String[]{""});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();

        connectToPort();

    }

    @Test
    public void testWinStatusOfRoundByLowerPlayerAction() {
        synchronized (this){
            try {
                wait(1000);

                playGame(String.valueOf(PlayerAction.LOWER));

                if (nextCardRankValue < baseCardRankValue)
                    Assert.assertTrue(finishRequest.isWin());

                if (nextCardRankValue >= baseCardRankValue)
                    Assert.assertFalse(finishRequest.isWin());

            } catch (Exception e){
            }
        }
    }

    @Test
    public void testWinStatusOfRoundByEqualsPlayerAction() {
        synchronized (this){
            try {
                wait(1000);

                playGame(String.valueOf(PlayerAction.EQUALS));

                if (nextCardRankValue == baseCardRankValue)
                    Assert.assertTrue(finishRequest.isWin());

                if (nextCardRankValue != baseCardRankValue)
                    Assert.assertFalse(finishRequest.isWin());

            } catch (Exception e){
            }
        }
    }

    @Test
    public void testWinStatusOfRoundByHigherPlayerAction() {
        synchronized (this){
            try {
                wait(1000);

                playGame(String.valueOf(PlayerAction.HIGHER));

                if (nextCardRankValue > baseCardRankValue)
                    Assert.assertTrue(finishRequest.isWin());

                if (nextCardRankValue <= baseCardRankValue)
                    Assert.assertFalse(finishRequest.isWin());

            } catch (Exception e){
            }
        }
    }

    private void playGame(String userGuess) throws IOException, ClassNotFoundException {

        Card baseCard = getBaseCard();
        playerAction(userGuess);
        FinishRoundRequest finishRequest = getFinishRoundRequest();
        Card nextCard = finishRequest.getCard();

        System.out.println("---START");
        System.out.println("Base Card - " + baseCard.getValue() + " of " + baseCard.getSuit());
        System.out.println("Your Guess - " + userGuess.toUpperCase());
        endRound(finishRequest);

        CardRank baseCardRank = baseCard.getValue();
        CardRank nextCardRank = nextCard.getValue();
        baseCardRankValue = CardRank.getValue(baseCardRank);
        nextCardRankValue = CardRank.getValue(nextCardRank);
    }

    private static void endRound(FinishRoundRequest finishRoundRequest){
        long roundID = finishRoundRequest.getRoundId();
        boolean win = finishRoundRequest.isWin();
        Card card = finishRoundRequest.getCard();

        if (card != null){
            CardSuit cardSuit = card.getSuit();
            CardRank cardRank = card.getValue();
            System.out.println("Next Card - " + cardRank + " of " + cardSuit);
        }

        if (win){
            System.out.println("You won");
            System.out.println("---END");
        } else {
            System.out.println("You lost");
            System.out.println("---END");
        }
    }

    private Card getBaseCard() throws IOException, ClassNotFoundException {
        StartRoundRequest request = (StartRoundRequest) in.readObject();
        return request.getBaseCard();
    }

    private void playerAction(String userGuess) {
//        str = "lower";
        PlayerActionRequest playerAction = new PlayerActionRequest(PlayerAction.valueOf(userGuess.toUpperCase()));
        sendPlayerAction(playerAction);
    }

    private FinishRoundRequest getFinishRoundRequest() throws IOException, ClassNotFoundException {
        return (FinishRoundRequest) in.readObject();
    }


    private void connectToPort() throws IOException, ClassNotFoundException {
        requestSocket = new Socket("localhost", 2004);
        nullEverything();
        System.out.println("Connected to localhost in port 2004 ");
        out = new ObjectOutputStream(requestSocket.getOutputStream());
        out.flush();
        in = new ObjectInputStream(requestSocket.getInputStream());
    }

    private void sendPlayerAction(PlayerActionRequest action) {
        try {
            out.writeObject(action);
            out.flush();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private void nullEverything() {
        gameClient = null;
        startRoundRequest = null;
        roundID = 0;
        timeStamp = 0;
        card = null;
        date = null;
        actionRoundDuration = 0;
        finishRoundRequest = null;
    }
}
