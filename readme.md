*Instructions*

You need to run ServerRunner class first. Then it will start to listen the given port. If the port is already
in use on your local machine, please change it.

Then you are required to run Client class. You can run Client class several times. All of them will connect
the port which server runs on. If the port is already in use on your local machine, please change it.

Be sure that ServerRunner and Client classes have same port variable. It is important.

Once you run both classes, server side send a random card to client side, and start to take for 10 seconds.
If during these 10 seconds user does not enter his/her guess, Client will not accept any other guess from user, 
and will stop. Like that, if Server does not accept any guess during 10 seconds from Client, it send FInishRoundRequest
to the Client.